import Inert from '@hapi/inert'
import Vision from '@hapi/vision'
import Blipp from 'blipp'
import Good from '@hapi/good'
import HapiSwagger from 'hapi-swagger'

import Pkg from '../../package'

/**
 * Loads plugins
 *
 * @param {Server} server
 * @returns {Promise<void>}
 */
export default async server => {
  const plugins = [
    { plugin: Inert, options: { } },
    { plugin: Vision },
    { plugin: Blipp },
    {
      plugin: Good,
      options: {
        ops: {
          interval: 5000
        },
        reporters: {
          console: [
            {
              module: '@hapi/good-squeeze',
              name: 'Squeeze',
              args: [
                {
                  log: '*',
                  response: '*',
                  request: '*',
                  error: '*'
                }
              ]
            },
            {
              module: '@hapi/good-console',
              args: [
                {
                  log: '*',
                  response: '*',
                  request: '*',
                  error: '*'
                }
              ]
            },
            'stdout'
          ]
        }
      }
    },
    {
      plugin: HapiSwagger,
      options: {
        cors: true,
        documentationPath: '/docs',
        info: {
          title: 'A hapi api',
          version: Pkg.version,
          description: Pkg.description
        }
      }
    }
  ]

  await server.register(plugins)
}
