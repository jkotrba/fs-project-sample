import Hapi from '@hapi/hapi'
import Relish from 'relish'
import jwksRsa from 'jwks-rsa'

import routes from './routes'
import configPlugins from './config/plugins'
import { bootstrap } from './db'

const port = Number(process.argv[2]) || process.env.PORT || 8280
process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const relish = Relish()

const validateUser = (decoded, request) => {
  if (decoded && decoded.sub) {
    if (decoded.scope) {
      return {
        isValid: true,
        credentials: {
          scope: decoded.scope.split(' '),
        },
      }
    }
    return { isValid: true }
  }
  return { isValid: false }
}

export default async () => {
  const server = Hapi.Server({
    port: port,
    router: {
      isCaseSensitive: false,
      stripTrailingSlash: true,
    },
    routes: {
      cors: true,
      validate: {
        failAction: relish.failAction,
      },
    },
  })

  await server.register(require('hapi-auth-jwt2'))

  server.auth.strategy('jwt', 'jwt', {
    complete: true,
    key: jwksRsa.hapiJwt2KeyAsync({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: `https://${process.env.AUTH_DOMAIN}/.well-known/jwks.json`,
    }),
    verifyOptions: {
      audience: 'https://fs-project-sample-api',
      issuer: `https://${process.env.AUTH_DOMAIN}/`,
      algorithms: ['RS256'],
    },
    validate: validateUser,
  })

  server.route(routes)

  await bootstrap.run()

  await configPlugins(server)

  await server.initialize()

  return server
}
