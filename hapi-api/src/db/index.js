import knex from './knex'

const run = async () => {
  const currentVersion = await knex.migrate.currentVersion()
  console.log(`migrations version: ${currentVersion}`)
  console.log('running db migrations')
  await knex.migrate
    .latest()
    .then(() => {
      console.log('running migrations complete')
      knex.migrate.currentVersion()
        .then(nextVersion => console.log(`migrations version: ${nextVersion}`))
    })
    .catch(error => {
      console.error('Failure running migrations', error)
    })
}

export const bootstrap = {
  run: run,
}
