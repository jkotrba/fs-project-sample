export const up = knex => {
  console.log('running up for 00001_add_addresses.js')
  return Promise.all([
    knex.schema.hasTable('address_type').then(exists => {
      if (!exists) {
        return knex.schema.createTable('address_type', table => {
          table.uuid('id').primary()
          table.string('name', 100)
          table.specificType('created_date', 'timestamptz').notNullable()
        })
      }
    }),
    knex.schema.hasTable('address').then(exists => {
      if (!exists) {
        return knex.schema.createTable('address', table => {
          table.uuid('id').primary()
          table.uuid('user_id').notNullable()
          table.uuid('address_type_id').notNullable()
          table.string('address_line1', 250).notNullable()
          table.string('address_line2', 250).notNullable()
          table.string('city', 100).notNullable()
          table.string('state_code', 2).notNullable()
          table.string('zip_code', 10).notNullable()
          table.specificType('created_date', 'timestamptz').notNullable()
          table
            .foreign('user_id')
            .references('id')
            .inTable('user')
          table
            .foreign('address_type_id')
            .references('id')
            .inTable('address_type')
        })
      }
    }),
  ])
}

export const down = knex => {
  return Promise.all([knex.schema.dropTable('address'), knex.schema.dropTable('address_type')])
}
