import Boom from '@hapi/boom'
import uuid from 'uuid/v4'
import knex from '../db/knex'

export const getAll = async (req, h) => {
  const dbData = await knex.select('id', 'email', 'handle', 'created_date').from('user')

  const dbUsers = dbData.map(_ => {
    return {
      id: _.id,
      email: _.email,
      handle: _.handle,
      created_date: _.created_date,
    }
  })
  return dbUsers
}

export const getSingle = async (req, h) => {
  const id = req.params.id

  const singleUser = await getSingleUser(id)

  if (singleUser) {
    return singleUser
  }

  return Boom.notFound()
}

export const add = async (req, h) => {
  const { email, handle } = req.payload
  const id = uuid()

  const newUser = {
    id,
    email,
    handle,
    created_date: new Date(),
  }

  await knex('user').insert(newUser)
  return { id }
}

export const update = async (req, h) => {
  const id = req.params.id
  const { email, handle } = req.payload

  const singleUser = await getSingleUser(id)

  if (!singleUser) {
    return Boom.notFound()
  }

  await knex('user')
    .where('id', id)
    .update({ email, handle })

  return h.response().code(200)
}

export const remove = async (req, h) => {
  const id = req.params.id

  const singleUser = await getSingleUser(id)

  if (!singleUser) {
    return Boom.notFound()
  }

  await knex('user')
    .del()
    .where('id', id)

  return h.response().code(200)
}

const getSingleUser = async id => {
  const singleUserData = await knex('user')
    .where({ id })
    .select('id', 'email', 'handle', 'created_date')

  const singleUser = singleUserData
    .map(_ => {
      return {
        id: _.id,
        email: _.email,
        handle: _.handle,
        created_date: _.created_date,
      }
    })
    .shift()
  return singleUser
}
