import auth0 from 'auth0-js'

class Auth {
  constructor() {
    this.auth0 = new auth0.WebAuth({
      clientID: `${process.env.REACT_APP_AUTH_CLIENTID}`,
      domain: `${process.env.REACT_APP_AUTH_DOMAIN}`,
      responseType: 'token id_token',
      audience: `${process.env.REACT_APP_AUTH_AUDIENCE}`,
      redirectUri: `${process.env.REACT_APP_AUTH_REDIRECT_URI}`,
      scope: `${process.env.REACT_APP_AUTH_SCOPE}`
    })
  }

  getProfile = () => {
    return this.profile
  }

  getIdToken = () => {
    return this.idToken
  }

  getAccessToken = () => {
    return this.accessToken
  }

  isAuthenticated = () => {
    return new Date().getTime() < this.expiresAt
  }

  signIn = () => {
    this.auth0.authorize()
  }

  handleAuthentication = () => {
    return new Promise((resolve, reject) => {
      this.auth0.parseHash((err, authResult) => {
        if (err) return reject(err)
        if (!authResult || !authResult.idToken) {
          return reject(err)
        }
        this.idToken = authResult.idToken
        this.profile = authResult.idTokenPayload
        this.accessToken = authResult.accessToken
        // set the time that the id token will expire at
        this.expiresAt = authResult.idTokenPayload.exp * 1000
        resolve()
      })
    })
  }

  signOut = () => {
    // clear id token, profile, and expiration
    this.idToken = null
    this.profile = null
    this.expiresAt = null
  }
}

const auth0Client = new Auth()

export default auth0Client
